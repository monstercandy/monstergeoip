require( "../../../MonsterCommon/lib/MonsterDependencies")(__dirname)

var ExpressTesterLib = require("ExpressTester")
var app = require( "../geoip-app.js")(ExpressTesterLib.moduleOptions)

ExpressTesterLib(app, function(mapi, assert){



	describe('generic lookups', function() {



        it('should resolve ip to country correctly', function(done) {

             mapi.post( "/lookup", {"ip": "178.48.246.208"}, function(err, result, httpResponse){
                // console.log(result)
                assert.deepEqual(result, {"country":"HU"})
                done()

             })

        })

        it('should resolve localhost as two dashes', function(done) {

             mapi.post( "/lookup", {"ip": "::ffff:127.0.0.1"}, function(err, result, httpResponse){
                // console.log(result)
                assert.deepEqual(result, {"country":"--"})
                done()

             })

        })

	})



}, 10000)


