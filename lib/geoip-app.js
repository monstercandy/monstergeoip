module.exports = function(moduleOptions) {
    moduleOptions = moduleOptions || {}
    moduleOptions.subsystem = "geoip"

    const LOCALHOST = "::ffff:127.0.0.1"

    var fs = require("MonsterDotq").fs();

    require( "../../MonsterCommon/lib/MonsterDependencies")(__dirname)

    const path = require('path')

    var config = require('MonsterConfig');
    var me = require('MonsterExpress');

    var ValidatorsLib = require("MonsterValidators")
    var vali = ValidatorsLib.ValidateJs()


    var geoip

    setupGeoip()


    config.appRestPrefix = "/geoip"

    var app = me.Express(config, moduleOptions);
    app.MError = me.Error

    app.enable("trust proxy")  // this application receives x-forwarded-for headers from other internal systems (typically relayer)


    var router = app.PromiseRouter(config.appRestPrefix)

    function validateAndResolve(in_data) {
         if(in_data.ip == LOCALHOST) return Promise.resolve("--")

         return vali.async(in_data, {
            ip: {presence:true, isValidIPv4: {canBeBroadcast: true}}
         })
         .then(d=>{
            var country = geoip.lookupCountry(d.ip)
            return Promise.resolve(country)
         })
    }

    router.post("/lookup", function(req,res,next){
       return validateAndResolve(req.body.json)
         .then(country=>{
            req.sendResponse({"country": country }); // => "US"
         })
    })



    const cron = require('Croner');

    var csp = app.config.get("cron_geoip_db_update")
    if(csp) {

        cron.schedule(csp, function(){
            return updateGeoip()
        });
    }


    return app


    function setupGeoip() {
      try {
         setupGeoipInternal()
      }catch(ex){
         console.log("error loading geoip", ex)
         updateGeoip()
      }
    }

    function setupGeoipInternal(){
       geoip = require('geoip-ultralight');
       geoip.startWatchingDataUpdate();
    }


    function updateGeoip(){

      console.log("updating geoip")

      const spawn = require('child_process').spawn;


        // 'cd','node_modules\\geoip-ultralight',';',
        // const update = spawn('cmd', ['/c','cd',';','npm','run-script','update-db']);
        const update = spawn('npm', ['run-script','updatedb'], {cwd: "node_modules/geoip-ultralight"});

        update.stdout.on('data', (data) => {
          console.log(data.toString());
        });

        update.stderr.on('data', (data) => {
          console.log(data.toString());
        });
        update.on('close', (code) => {
          console.log(`geoip update process exited with code ${code}`);
          app.InsertEvent(null, {e_event_type: "data-updated"});
          if(!geoip)
            setupGeoipInternal()
        });
   }

}
