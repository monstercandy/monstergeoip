var geoip = require('geoip-ultralight');
geoip.startWatchingDataUpdate();

// Unlike geoip-lite's lookup() call, geoip-ultralight exposes
// a lookupCountry() function to avoid confusion
setInterval(function(){
  var ip = "207.97.227.239";
  console.log(geoip.lookupCountry(ip)); // => "US"
}, 2000)

doUpdate()

function doUpdate(){

	const spawn = require('child_process').spawn;

	setTimeout(function(){
	    console.log("doing update!")
		
		// 'cd','node_modules\\geoip-ultralight',';',
		// const update = spawn('cmd', ['/c','cd',';','npm','run-script','update-db']);
		const update = spawn('cmd', ['/c','npm','run-script','updatedb'], {cwd: "node_modules\\geoip-ultralight"});
		
		update.stdout.on('data', (data) => {
		  console.log(data.toString());
		});

		update.stderr.on('data', (data) => {
		  console.log(data.toString());
		});
		update.on('close', (code) => {
		  console.log(`child process exited with code ${code}`);
		  doUpdate()
		});		
	}, 10000)

}