FROM monstercommon

ADD lib /opt/MonsterGeoip/lib/
ADD etc /etc/monster/
RUN INSTALL_DEPS=1 /opt/MonsterGeoip/lib/bin/geoip-install.sh && /opt/MonsterGeoip/lib/bin/geoip-test.sh

ENTRYPOINT ["/opt/MonsterGeoip/lib/bin/geoip-start.sh"]
